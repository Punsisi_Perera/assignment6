#include<stdio.h>

void pattern(int n);

int main(){

	int n = 4;
	pattern(n);
	return 0;
}

void pattern(int n){

	if(n==0)
	{
		return;
	}
	else
	{
		pattern(n-1);

		for(int i= n; i>=1;i--){
			printf("%d",i);
		}
		printf("\n");
	}
}
